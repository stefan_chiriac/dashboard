$(document).ready(function() {
  $('#dataTable').DataTable( {
    dom: 'Bfrtip',
    buttons: [
      {
        extend: 'csv',
        text: 'Export to CSV',
        className: 'd-none d-sm-inline-block btn btn-sm btn-primary shadow-sm'
      }
    ]
  } );
} );


