<?php

require($_SERVER['DOCUMENT_ROOT'] . "/config/config.php");

class ExtractorService
{

    protected $db;

    public function __construct()
    {
        $this->db = Config();
    }

    public function __destruct()
    {
        $this->db = null;
    }

    public function getTodayFinishedClientOrderTypeDeliveries()
    {
        $query = $this->db->prepare("select COUNT(DISTINCT d.id) as 'client_orders'
from delivery d
         inner join delivery_line dl on d.id = dl.delivery_id
         inner join delivery_line_quantity dlq on dl.id = dlq.delivery_line_id
         inner join delivery_reference dr on dl.delivery_id = dr.delivery_id
where dlq.virtual_storage_id = 8
  and dr.type = 'id_comanda'
  and dlq.created > CURDATE()");
        $query->execute();
        $data = array();
        while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
            $data[] = $row;
        }
        return $data;
    }


    public function getTodayFinishedTransferTypeDeliveries()
    {
        $query = $this->db->prepare("select COUNT(DISTINCT d.id) as 'transfers'
from delivery d
         inner join delivery_line dl on d.id = dl.delivery_id
         inner join delivery_line_quantity dlq on dl.id = dlq.delivery_line_id
         inner join delivery_reference dr on dl.delivery_id = dr.delivery_id
where dlq.virtual_storage_id = 8
  and dr.type = 'id_transfer_delivery'
  and dlq.created > CURDATE()");
        $query->execute();
        $data = array();
        while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
            $data[] = $row;
        }
        return $data;
    }

    public function getTodayTotalDeliveries()
    {
        $query = $this->db->prepare("select COUNT(DISTINCT d.id) as 'total_deliveries'
from delivery d
         inner join delivery_line dl on d.id = dl.delivery_id
         inner join delivery_line_quantity dlq on dl.id = dlq.delivery_line_id
         inner join delivery_reference dr on dl.delivery_id = dr.delivery_id
where dlq.virtual_storage_id = 8
  and dlq.created > CURDATE()");
        $query->execute();
        $data = array();
        while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
            $data[] = $row;
        }
        return $data;
    }

    public function getTodayTotalTasks()
    {
        $query = $this->db->prepare("select COUNT(DISTINCT t.id) as 'total_tasks'
from delivery d
         inner join delivery_line dl on d.id = dl.delivery_id
         inner join delivery_line_quantity dlq on dl.id = dlq.delivery_line_id
         inner join task t on dlq.generate_awb_task_id = t.id
where dlq.virtual_storage_id = 8
  and dlq.created > CURDATE();");
        $query->execute();
        $data = array();
        while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
            $data[] = $row;
        }
        return $data;
    }

    public function getTodayClientOrderTasks()
    {
        $query = $this->db->prepare("select Count(t.id)as 'total_client_tasks'
    from delivery d
         inner join delivery_line dl on d.id = dl.delivery_id
         inner join delivery_line_quantity dlq on dl.id = dlq.delivery_line_id
         inner join delivery_reference dr on dl.delivery_id = dr.delivery_id
        inner join task t on dlq.generate_awb_task_id = t.id
where dlq.virtual_storage_id = 8
  and dr.type = 'id_comanda'
  and dlq.created > CURDATE()");
        $query->execute();
        $data = array();
        while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
            $data[] = $row;
        }
        return $data;
    }

    public function getPendingDeliveries()
    {
        $query = $this->db->prepare("select Count(t.id)as 'pending_deliveries'
    from delivery d
         inner join delivery_line dl on d.id = dl.delivery_id
         inner join delivery_line_quantity dlq on dl.id = dlq.delivery_line_id
         inner join delivery_reference dr on dl.delivery_id = dr.delivery_id
        inner join task t on dlq.picking_task_id = t.id
where dlq.virtual_storage_id = 8
  and dr.type = 'id_transfer_delivery'
  and t.status = 'finished'
  and dlq.created > CURDATE()");
        $query->execute();
        $data = array();
        while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
            $data[] = $row;
        }
        return $data;
    }

    public function getFirstTenMostDeliveredProducts()
    {
        $query = $this->db->prepare("select pn.name,dl.product_id, count(*) AS Frequency
from delivery d
         inner join delivery_line dl on d.id = dl.delivery_id
         inner join delivery_line_quantity dlq on dl.id = dlq.delivery_line_id
            inner join wms_common_rbh.product_name pn on dl.product_id = pn.product_id
where dlq.virtual_storage_id = 8
  and dlq.created > CURDATE()
  and pn.country_id = 1
group by dl.product_id
order by COUNT(*) DESC
LIMIT 10");
        $query->execute();
        $data = array();
        while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
            $data[] = $row;
        }
        return $data;
    }

    public function getProductsListOfTodayClientOrders()
    {
        {
            $query = $this->db->prepare("select d.source_application_reference as 'delivery_reference',
       pn.name as 'product_name',
       dl.product_id as 'product_id',
       dl.created as 'delivery_created'
from delivery d
         inner join delivery_line dl on d.id = dl.delivery_id
         inner join delivery_line_quantity dlq on dl.id = dlq.delivery_line_id
         inner join delivery_reference dr on dl.delivery_id = dr.delivery_id
         inner join wms_common_rbh.product_name pn on dl.product_id = pn.product_id
where dlq.virtual_storage_id = 8
and pn.country_id = 1
  and dr.type = 'id_comanda'
  and dlq.created > CURDATE()");
            $query->execute();
            $data = array();
            while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
                $data[] = $row;
            }
            return $data;
        }
    }

    public function getProductsListOfTodayTransfers()
    {
        {
            $query = $this->db->prepare("select d.source_application_reference as 'delivery_reference',
       pn.name as 'product_name',
       dl.product_id as 'product_id',
       dl.created as 'delivery_created'
from delivery d
         inner join delivery_line dl on d.id = dl.delivery_id
         inner join delivery_line_quantity dlq on dl.id = dlq.delivery_line_id
         inner join delivery_reference dr on dl.delivery_id = dr.delivery_id
         inner join wms_common_rbh.product_name pn on dl.product_id = pn.product_id
where dlq.virtual_storage_id = 8
and pn.country_id = 1
  and dr.type = 'id_transfer_delivery'
  and dlq.created > CURDATE()");
            $query->execute();
            $data = array();
            while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
                $data[] = $row;
            }
            return $data;
        }
    }

    public function getTodayClientOrderTasksList()
    {
        $query = $this->db->prepare("select t.id as 'task_id',

    from delivery d
         inner join delivery_line dl on d.id = dl.delivery_id
         inner join delivery_line_quantity dlq on dl.id = dlq.delivery_line_id
         inner join delivery_reference dr on dl.delivery_id = dr.delivery_id
        inner join task t on dlq.generate_awb_task_id = t.id
where dlq.virtual_storage_id = 8
  and dr.type = 'id_comanda'
  and dlq.created > CURDATE()");
        $query->execute();
        $data = array();
        while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
            $data[] = $row;
        }
        return $data;
    }
}